function tictactoe() {
    squares = [];
    NROWS = NCOLS = 3;
    turns = NROWS*NCOLS;

    $.Player = function($) {
        this.piecePositions = new Array(NROWS);
        for (var i=0; i<this.piecePositions.length; i++) {
            this.piecePositions[i] = new Array(NCOLS);
        }
    }

    $.Player.prototype.placePiece = function(sqId,piece) {
        if ($('#'+sqId).text() == '') {
            $('#'+sqId).text(piece);
            var i = $('#'+sqId).parent().attr('id');
            i = parseInt(i.charAt(1));
            var j = $('#'+sqId).attr('class');
            j = parseInt(j.charAt(1));
            this.piecePositions[i][j] = 1;
            squares[sqId] = piece;
            turns--;
        }
        else {
            alert('Choose another square. You have already placed a piece on this square');
        }
        //alert(this.piecePositions)
    }

    $.Player.prototype.isRowWin = function() {
        isWin = false;
        for (var i=0; i<NROWS; i++)
        {
            var x = 1;
            for (var j=0; j<NCOLS; j++)
            {
                x = x && this.piecePositions[i][j];
            }
            if (x) isWin = true;
        }
        return isWin;
    };

    $.Player.prototype.isColWin = function() {
        isWin = false;
        for (var i=0; i<NROWS; i++)
        {
            var x = 1;
            for (var j=0; j<NCOLS; j++)
            {
                x = x && this.piecePositions[j][i];
            }
            if (x) isWin = true;
        }
        return isWin;
    };

    $.Player.prototype.isLeftDiagonalWin = function() {
        isWin = false;
        var x = 1;
        for (var i=0; i<NROWS; i++)
        {
            x = x && this.piecePositions[i][i];
        }
        if (x) isWin = true;
        return isWin;
    };

    $.Player.prototype.isRightDiagonalWin = function() {
        isWin = false;
        var x = 1;
        var j = NCOLS-1;
        for (i=0; i<NROWS; i++)
        {
            x = x && this.piecePositions[i][j];
            j--;
        }
        if (x) isWin = true;
        return isWin;
    };

    var human = new $.Player();
    var computer = new $.Player();

    $('td').click (
        function() {
            human.placePiece($(this).attr('id'),'X');

            if (human.isRowWin() || human.isColWin() || human.isLeftDiagonalWin() || human.isRightDiagonalWin()) {
                if ( alert('Player wins the board') ) {}
                else {
                    window.location.reload();
                }
            }
            else if (computer.isRowWin() || computer.isColWin() || computer.isLeftDiagonalWin() || computer.isRightDiagonalWin()) {
                if ( alert('Computer wins the board') ) {}
                else {
                    window.location.reload();
                }
            }
            else if (turns == 0) {
                if ( alert('No one wins the board. Game is a tie') ) {}
                else {
                    window.location.reload();
                }
            }
            else {
                var tempArr = [];
                for (var i = 0; i < NROWS*NCOLS; i++) {
                    if (squares[i] != 'X' && squares[i] != 'O') {
                        tempArr.push(i);
                    }
                }
                var compSquare = tempArr[Math.floor(Math.random() * tempArr.length)];
                computer.placePiece(compSquare,'O');
                if (computer.isRowWin() || computer.isColWin() || computer.isLeftDiagonalWin() || computer.isRightDiagonalWin()) {
                    if ( alert('Computer wins the board') ) {}
                    else {
                        window.location.reload();
                    }
                }
            }
            /*tempArr = tempArr.filter(
                function(elem) {
                    return elem != compSquare;
                }
            );*/
        }
    );
}